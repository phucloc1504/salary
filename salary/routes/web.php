<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//User
Route::resource('/profile','User\ProfileController');
// Route::resource('/timesheets','User\TimesheetsController');
// Route::resource('/days-off','User\DaysOffController');
// Route::resource('/late-hours','User\LateHoursController');
// Route::resource('/overtime','User\OvertimeController');


Route::get('/', 'HomeController@index')->name('home');
//login
Route::get('/user-login', 'Auth\LoginController@index')->name('user-login');
// Route::get('/user-register', 'Auth\RegisterController@index')->name('user-register');
// Route::get('/user-reset', 'Auth\ResetPasswordController@index')->name('user-reset');

//admin
Route::resource('/admin-home','Admin\HomeController');
// Route::resource('/admin-days-off','Admin\DaysOffController');
// Route::resource('/admin-late-hours','Admin\LateHoursController');
// Route::resource('/admin-overtime','Admin\OvertimeController');
// Route::resource('/admin-salary','Admin\SalaryController');
