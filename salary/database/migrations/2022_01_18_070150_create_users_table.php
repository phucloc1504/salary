<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('phone');
            $table->string('email');
            $table->string('address');
            $table->string('title');
            $table->string('username');
            $table->string('password');
            $table->date('start_date');
            $table->enum('status', array('0','1'))->default('0');
            $table->integer('basic_salary_id')->unsigned();
            $table->foreign('basic_salary_id')->references('id')->on('basic_salary')->onDelete('cascade')->onUpdate('cascade');
            $table->integer('insurance_policy_id')->unsigned();
            $table->foreign('insurance_policy_id')->references('id')->on('insurance_policy')->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
