<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class History extends Model
{
    use SoftDeletes;// add soft delete
    //
    protected $table='history';
    protected $fillable =
        [
            'id',
            'user_id',
            'admin_id',
            'content',
            'create_at',
            'update_at',
            'deleted_at'
        ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
