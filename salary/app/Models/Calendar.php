<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Calendar extends Model
{
    use SoftDeletes;// add soft delete
    //
    protected $table='calendar';
    protected $fillable =
        [
            'id',
            'type',
            'start_time',
            'end_time',
            'content',
            'user_id',
            'create_at',
            'update_at',
            'deleted_at'
        ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
