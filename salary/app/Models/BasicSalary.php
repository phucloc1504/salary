<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BasicSalary extends Model
{
    use SoftDeletes;// add soft delete
    //
    protected $table='basic_salary';
    protected $fillable =
        [
            'id',
            'coefficients_salary',
            'basic_salary',
            'create_at',
            'update_at',
            'deleted_at'
        ];
    public function user()
    {
        return $this->hasMany(User::class, 'basic_salary_id', 'id');
    }
}
