<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InsurancePolicy extends Model
{
    use SoftDeletes;// add soft delete
    //
    protected $table='insurance_policy';
    protected $fillable =
        [
            'id',
            'money_for_ip',
            'status',
            'payment_date',
            'create_at',
            'update_at',
            'deleted_at'
        ];
    public function user()
    {
        return $this->hasMany(User::class, 'insurance_policy_id', 'id');
    }
}
