<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model
{
    use SoftDeletes;// add soft delete
    //
    protected $table='users';
    protected $fillable =
        [
            'id',
            'name',
            'phone',
            'email',
            'address',
            'title',
            'username',
            'password',
            'start_date',
            'basic_salary_id',
            'insurance_policy_id',
            'create_at',
            'update_at',
            'deleted_at'
        ];

    public function basic_salary()
    {
        return $this->belongsTo(BasicSalary::class, 'basic_salary_id', 'id');
    }

    public function insurance_policy()
    {
        return $this->belongsTo(InsurancePolicy::class, 'insurance_policy_id', 'id');
    }

    public function salary()
    {
        return $this->hasMany(Salary::class, 'user_id', 'id');
    }
    public function calendar()
    {
        return $this->hasMany(Calendar::class, 'user_id', 'id');
    }
    public function history()
    {
        return $this->hasMany(History::class, 'user_id', 'id');
    }
}
