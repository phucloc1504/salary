<!doctype html>
<html lang="en">


<!-- Mirrored from creatantech.com/demos/codervent/laravel-vertical/index by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 21 Apr 2021 01:09:00 GMT -->
<!-- Added by HTTrack --><meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->
<head>
	<!-- Required meta tags -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!--favicon-->
    <base href="{{ asset('') }}admin_layout/">
	<link rel="icon" href="assets/images/favicon-32x32.png" type="image/png" />
	<!--plugins-->
		<link href="assets/plugins/vectormap/jquery-jvectormap-2.0.2.css" rel="stylesheet"/>
		<link href="assets/plugins/simplebar/css/simplebar.css" rel="stylesheet" />
	<link href="assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" />
	<link href="assets/plugins/metismenu/css/metisMenu.min.css" rel="stylesheet" />
	<!-- loader-->
	<link href="assets/css/pace.min.css" rel="stylesheet" />
	<script src="assets/js/pace.min.js"></script>
	<!-- Bootstrap CSS -->
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500&amp;display=swap" rel="stylesheet">
	<link href="assets/css/app.css" rel="stylesheet">
	<link href="assets/css/icons.css" rel="stylesheet">

	<title>Dashtreme - Multipurpose Bootstrap5 Admin Template</title>
</head>

<body class="bg-theme bg-theme1">
	<!--wrapper-->
    <div id="app">
        <div class="wrapper">

            <div><header-admin/></div>
            <div class="page-wrapper">
                <router-view></router-view>
                {{-- @yield('content') --}}
            </div>
            <!--start overlay-->
            <div class="overlay toggle-icon"></div>
            <!--end overlay-->
            <!--Start Back To Top Button--> <a href="javaScript:;" class="back-to-top"><i class='bx bxs-up-arrow-alt'></i></a>
            <!--End Back To Top Button-->

            <footer class="page-footer">
                <p class="mb-0">Copyright © 2021. All right reserved.</p>
            </footer>
        </div>
        <!--end wrapper-->
        <!--start switcher-->
        <div class="switcher-wrapper">
            <div class="switcher-btn"> <i class='bx bx-cog bx-spin'></i>
            </div>
            <div class="switcher-body">
                <div class="d-flex align-items-center">
                    <h5 class="mb-0 text-uppercase">Theme Customizer</h5>
                    <button type="button" class="btn-close ms-auto close-switcher" aria-label="Close"></button>
                </div>
                <hr/>
                <p class="mb-0">Gaussian Texture</p>
                  <hr>

                  <ul class="switcher">
                    <li id="theme1"></li>
                    <li id="theme2"></li>
                    <li id="theme3"></li>
                    <li id="theme4"></li>
                    <li id="theme5"></li>
                    <li id="theme6"></li>
                  </ul>
                   <hr>
                  <p class="mb-0">Gradient Background</p>
                  <hr>

                  <ul class="switcher">
                    <li id="theme7"></li>
                    <li id="theme8"></li>
                    <li id="theme9"></li>
                    <li id="theme10"></li>
                    <li id="theme11"></li>
                    <li id="theme12"></li>
                    <li id="theme13"></li>
                    <li id="theme14"></li>
                    <li id="theme15"></li>
                  </ul>
            </div>
        </div>
        <!--end switcher-->
        <!-- Bootstrap JS -->
        {{-- <script src="{{ asset('/admin_layout/assets/js/bootstrap.bundle.min.js') }}"></script> --}}
        <!--plugins-->
        {{-- <script src="{{ asset('/admin_layout/assets/js/jquery.min.js') }}"></script> --}}
        {{-- <script src="{{ asset('/admin_layout/assets/js/assets/plugins/simplebar/js/simplebar.min.js') }}"></script> --}}
        {{-- <script src="{{ asset('/admin_layout/assets/js/assets/plugins/metismenu/js/metisMenu.min.js') }}"></script> --}}
        {{-- <script src="{{ asset('/admin_layout/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js') }}"></script> --}}
        <!--app JS-->
        {{-- <script src="{{ asset('/admin_layout/assets/js/app.js') }}"></script> --}}
            {{-- <script src="{{ asset('/admin_layout/assets/plugins/chartjs/js/Chart.min.js') }}"></script>
            <script src="{{ asset('/admin_layout/assets/plugins/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
            <script src="{{ asset('/admin_layout/assets/plugins/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
            <script src="{{ asset('/admin_layout/assets/plugins/jquery.easy-pie-chart/jquery.easypiechart.min.js') }}"></script>
            <script src="{{ asset('/admin_layout/assets/plugins/sparkline-charts/jquery.sparkline.min.js') }}"></script>
            <script src="{{ asset('/admin_layout/assets/plugins/jquery-knob/excanvas.js') }}"></script>
            <script src="{{ asset('/admin_layout/assets/plugins/jquery-knob/jquery.knob.js') }}"></script> --}}
            {{-- <script>
              $(function() {
                  $(".knob").knob();
              });
            </script> --}}
            {{-- <script src="{{ asset('/admin_layout/assets/js/index.js') }}"></script> --}}
    </div>

	</body>

    <script src="{{ mix('js/app.js') }}"></script>
<!-- Mirrored from creatantech.com/demos/codervent/laravel-vertical/index by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 21 Apr 2021 01:10:57 GMT -->
</html>
