<!DOCTYPE HTML>
<html lang="en">


<!-- Mirrored from duruthemes.com/demo/html/lonon/light/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 14 Jan 2022 06:00:03 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Lonon</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <base href="{{ asset('') }}frontend/">
    <link rel="icon" type="image/png" href="images/favicon.png" />
    <link href="https://fonts.googleapis.com/css?family=Rambla:400,700&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/et-lineicons.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">

    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/2.0.5/waypoints.min.js"></script>
    <script src="{{ asset('/frontend/js/noframework.waypoints.min.js') }}"></script>

    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144098545-1"></script>
    <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', 'UA-144098545-1');
    </script>

</head>
<body>
    <div id="app">
        <div id="lonon-page"> <a href="#" class="js-lonon-nav-toggle lonon-nav-toggle"><i></i></a>
            <!-- Sidebar Section -->
            <aside id="lonon-aside">
                <header-layout/>

            </aside>
            <!-- Main Section -->
            <div id="lonon-main">
                <router-view></router-view>
                {{-- @yield('main') --}}
                <!-- Footer -->
                <div id="lonon-footer2">
                    <div class="lonon-narrow-content">
                        <div class="row">
                            <div class="col-md-4 animate-box" data-animate-effect="fadeInLeft">
                                <p class="lonon-lead">&copy; 2021 Lonon. All rights reserved</p>
                            </div>
                            <div class="col-md-4 animate-box" data-animate-effect="fadeInLeft">
                                <h2 class="text-center">Lonon F. Smith</h2> </div>
                            <div class="col-md-4 animate-box" data-animate-effect="fadeInLeft">
                                <ul class="social-network">
                                    <li><a href="#"><i class="ti-facebook font-14px black-icon"></i></a></li>
                                    <li><a href="#"><i class="ti-twitter-alt font-14px black-icon"></i></a></li>
                                    <li><a href="#"><i class="ti-instagram font-14px black-icon"></i></a></li>
                                    <li><a href="#"><i class="ti-linkedin font-14px black-icon"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Js -->
            {{-- <script src="/js/jquery.min.js"></script>
            <script src="/js/modernizr-2.6.2.min.js"></script>
            <script src="/js/jquery.easing.1.3.js"></script>
            <script src="/js/bootstrap.min.js"></script>
            <script src="/js/jquery.waypoints.min.js"></script>
            <script src="/js/sticky-kit.min.js"></script> --}}

        </div>
    </div>

</body>
            <script src="{{ asset('/frontend/js/jquery.min.js') }}"></script>
            {{-- <script src="{{ asset('/frontend/js/modernizr-2.6.2.min.js') }}"></script>
            <script src="{{ asset('/frontend/js/jquery.easing.1.3.js') }}"></script> --}}
            <script src="{{ asset('/frontend/js/bootstrap.min.js') }}"></script>
            {{-- <script src="{{ asset('/frontend/js/jquery.waypoints.min.js') }}"></script> --}}
            {{-- <script src="{{ asset('/frontend/js/main.js') }}"></script> --}}


<script src="{{ mix('js/app.js') }}"></script>
</html>
