<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from brandio.io/envato/iofrm/html/login18.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 17 Jan 2022 13:26:54 GMT -->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>iofrm</title>
    <base href="{{ asset('') }}login_layout/">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" type="text/css" href="css/iofrm-style.css">
    <link rel="stylesheet" type="text/css" href="css/iofrm-theme18.css">
</head>
<body>
    <div id="app">
        <div class="form-body without-side">
            <div class="website-logo">
                <a href="index.html">
                    <div class="logo">
                        <img class="logo-size" src="images/logo-light.svg" alt="">
                    </div>
                </a>
            </div>
            <div class="row">
                <div class="img-holder">
                    <div class="bg"></div>
                    <div class="info-holder">
                        <img src="images/graphic3.svg" alt="">
                    </div>
                </div>
                <div class="form-holder">
                    <router-view></router-view>
                    {{-- @yield('content') --}}
                </div>
            </div>
        </div>
    </div>
<script src="{{ asset('/login_layout/js/jquery.min.js') }}"></script>
<script src="{{ asset('/login_layout/js/popper.min.js') }}"></script>
<script src="{{ asset('/login_layout/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('/login_layout/js/main.js') }}"></script>
</body>

<script src="{{ mix('js/app.js') }}"></script>
<!-- Mirrored from brandio.io/envato/iofrm/html/login18.html by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 17 Jan 2022 13:26:58 GMT -->
</html>
