/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */


require('./bootstrap');

window.Vue = require('vue');
import router from './router';
/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

// Vue.component('example-component', require('./components/ExampleComponent.vue').default);

// User
Vue.component('Profile', require('./components/User/Profile.vue').default);
Vue.component('Timesheets', require('./components/User/Timesheets.vue').default);
Vue.component('days-off', require('./components/User/DaysOff.vue').default);
Vue.component('late-hours', require('./components/User/LateHours.vue').default);
Vue.component('overtime', require('./components/User/Overtime.vue').default);

Vue.component('header-layout', require('./components/ViewLayouts/HeaderLayout.vue').default);

// Admin
Vue.component('accounts', require('./components/Admin/Account.vue').default);
Vue.component('admin-days-off', require('./components/Admin/DaysOffAdmin.vue').default);
Vue.component('admin-late-hours', require('./components/Admin/LateHoursAdmin.vue').default);
Vue.component('admin-overtime', require('./components/Admin/OvertimeAdmin.vue').default);
Vue.component('admin-salary', require('./components/Admin/Salary.vue').default);

Vue.component('header-admin', require('./components/ViewLayouts/HeaderAdmin.vue').default);

// Login
Vue.component('login', require('./components/Login/Login.vue').default);
Vue.component('register', require('./components/Login/Register.vue').default);
Vue.component('reset', require('./components/Login/Reset.vue').default);
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */



// const app = new Vue({
//     el: '#app',
// });

const app = new Vue({
    el: '#app',
    router
});

