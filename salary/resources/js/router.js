import Vue from 'vue';
import Router from 'vue-router';
Vue.use(Router);
import Profile from './components/User/Profile';
import LateHours from './components/User/LateHours';
import Timesheets from './components/User/Timesheets';
import DaysOff from './components/User/DaysOff';
import Overtime from './components/User/Overtime';

import Account from './components/Admin/Account';
import DaysOffAdmin from './components/Admin/DaysOffAdmin';
import LateHoursAdmin from './components/Admin/LateHoursAdmin';
import OvertimeAdmin from './components/Admin/OvertimeAdmin';
import Salary from './components/Admin/Salary';

import Login from './components/Login/Login';
import Register from './components/Login/Register';
import Reset from './components/Login/Reset';

const routes =[
    //user
    {
        path: '/profile',
        component: Profile,
        name: 'vue-profile'
    },
    {
        path: '/latehours',
        component: LateHours,
        name: 'vue-latehours'
    },
    {
        path: '/timesheets',
        component: Timesheets,
        name: 'vue-timesheets'
    },
    {
        path: '/daysoff',
        component: DaysOff,
        name: 'vue-daysoff'
    },
    {
        path: '/overtime',
        component: Overtime,
        name: 'vue-overtime'
    },
    //admin
    {
        path: '/account',
        component: Account,
        name: 'vue-account'
    },
    {
        path: '/daysoff-admin',
        component: DaysOffAdmin,
        name: 'vue-daysoff-admin'
    },
    {
        path: '/latehours-admin',
        component: LateHoursAdmin,
        name: 'vue-latehours-admin'
    },
    {
        path: '/overtime-admin',
        component: OvertimeAdmin,
        name: 'vue-overtime-admin'
    },
    {
        path: '/salary-admin',
        component: Salary,
        name: 'vue-salary'
    },
    //login
    {
        path: '/login',
        component: Login,
        name: 'vue-login'
    },
    {
        path: '/register',
        component: Register,
        name: 'vue-register'
    },
    {
        path: '/reset',
        component: Reset,
        name: 'vue-reset'
    },
]

export default new Router({
    // mode: 'history',
    routes
})
